--- README  -------------------------------------------------------------

This module adds new formatter for image field,
print div with the image background.

--- INSTALLATION --------------------------------------------------------

- Install like a standard Drupal module.
- Go to your field display settings and select Image Background Formatter.
